<?php

namespace snitch\authevesso\migrations;

class add_table_100 extends \phpbb\db\migration\migration
{
    /**
     * This migration depends on phpBB's v314 migration
     * already being installed.
     */
    static public function depends_on()
    {
        return array('\snitch\authevesso\migrations\add_module_100');
    }

    public function update_schema()
    {
        return array(
            'add_columns'        => array(
                $this->table_prefix . 'users'        => array(
                    'user_refreshtoken'    => array('VCHAR:255', NULL),
                    'user_characterid'    => array('BINT', NULL),
                    'user_lastapi' => array('TIMESTAMP', NULL),
                    'user_apifailcount' => array('UINT', NULL),
                ),
                $this->table_prefix . 'sessions'        => array(
                    'session_authstate'    => array('VCHAR:255', NULL),
                    'session_refreshtoken'    => array('VCHAR:255', NULL),
                    'session_characterid'    => array('BINT', NULL),
                    'session_charactername'    => array('VCHAR:100', NULL),
                ),
            ),

            'add_tables'    => array(
                $this->table_prefix . 'authevesso_groups' => array(
                    'COLUMNS' => array(
                        'id'                => array('BINT', NULL),
                        'name'              => array('VCHAR_UNI:255', ''),
                        'type'              => array('VCHAR:20', ''),
                        'forum_group'       => array('UINT', NULL),
                        'ts_group'          => array('UINT', NULL),
                    ),
                ),
            ),
        );
    }

    public function revert_schema()
    {
        return array(
            'drop_columns'        => array(
                $this->table_prefix . 'users'        => array(
                    'user_refreshtoken',
                    'user_characterid',
                    'user_lastapi',
                    'user_apifailcount',
                ),
                $this->table_prefix . 'sessions'        => array(
                    'session_authstate',
                    'session_refreshtoken',
                    'session_characterid',
                    'session_charactername',
                ),
            ),

            'drop_tables'    => array(
                $this->table_prefix . 'authevesso_groups',
            ),
        );
    }

}
